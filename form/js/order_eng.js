var serviceType = "";
var name = "";
var email = "";
var phone = "";
var date = "";
var additionalThings = "";

$(document).ready(function(){

  $( "#datepicker1" ).datepicker({ minDate: 1});

  var $datepicker = $( "#datepicker1" );
  $datepicker.datepicker();
  var myDate = new Date();
  myDate.setDate(myDate.getDate() + 2);
  $datepicker.datepicker('setDate', myDate);

  $( "#datepicker1" ).click(function() {
    $(this).blur();
  });

  $( ".serviceType" ).click(function() {
    $(".serviceType").css({"border-color" : "", "box-shadow" : "", "outline" : ""});
    $(this).parent().find(".warning").hide();
    $("#serviceTypeLabel").css("color", "#687177");
    $(".tester").show();
    $(".serviceType").css("background-color", "#ffffff");
    $(".serviceType").css("color", "#000000");
    serviceType = $(this).val();
    $( this ).css("background-color", "#1FBAD6");
    $( this ).css("color", "#ffffff");
    if ( $(this).val() == "8H") {
      $(".tester").html("Total: ₩75,000");
    } else {
      $(".tester").html("Total: ₩45,000");
    }
  });

  $("input#name, input#email, input#phone, input#address").bind("change paste keyup", function() {
    $(this).css({"border-color" : "", "box-shadow" : "", "outline" : ""});
    $(this).parent().parent().find("label").css("color","#687177");
    $(this).parent().find(".warning").hide();
  });

  $( ".placeOrder" ).click(function() {
    name = $("input#name").val();
    address = $("input#address").val();
    phone = $("input#phone").val();
    date = $("input#datepicker1").val();
    additionalThings = $("textarea#additionalThings").val();
    if (serviceType == "" || name == "" || address == "" || phone == "" || date == "") {

      if(serviceType == "") {
        $(".serviceType").css({"border-color" : "rgba(255,0,0,0.8)", "box-shadow" : "0 1px 1px rgba(255, 0, 0, 0.075) inset, 0 0 8px rgba(255, 0, 0, 0.6)", "outline" : "0 none"});

        if( $(".serviceType").parent().find(".warning").length == 0 ) {
          $(".serviceType").parent().append("<p style='color:red;' class='warning'>Select a Service Type</p>");
        }
        $("#serviceTypeLabel").css("color", "red");

      }

      if(name == "") {
        $("input#name").css({"border-color" : "rgba(255,0,0,0.8)", "box-shadow" : "0 1px 1px rgba(255, 0, 0, 0.075) inset, 0 0 8px rgba(255, 0, 0, 0.6)", "outline" : "0 none"});
        $("#nameLabel").css("color", "red");
        if( $("input#name").parent().find(".warning").length == 0 ) {
          $("input#name").parent().append("<p style='color:red;' class='warning'>Name is required</p>");
        }
      }

      if(phone == "") {
        $("input#phone").css({"border-color" : "rgba(255,0,0,0.8)", "box-shadow" : "0 1px 1px rgba(255, 0, 0, 0.075) inset, 0 0 8px rgba(255, 0, 0, 0.6)", "outline" : "0 none"});
        $("#phoneLabel").css("color", "red");

        if( $("input#phone").parent().find(".warning").length == 0 ) {
          $("input#phone").parent().append("<p style='color:red;' class='warning'>Phone number is required</p>");
        }
      }

      if(address == "") {
        $("input#address").css({"border-color" : "rgba(255,0,0,0.8)", "box-shadow" : "0 1px 1px rgba(255, 0, 0, 0.075) inset, 0 0 8px rgba(255, 0, 0, 0.6)", "outline" : "0 none"});
        $("#addressLabel").css("color", "red");

        if( $("input#address").parent().find(".warning").length == 0 ) {
          $("input#address").parent().append("<p style='color:red;' class='warning'>Address is required</p>");
        }
      }

    } else {
      console.log(name+email+phone+date+address+additionalThings);
      $.ajax({
        url: "././mail/order.php",
        type: "POST",
        data: {
          serviceType: serviceType,
          name: name,
          address: address,
          phone: phone,
          date: date,
          additionalThings: additionalThings
        },
        dataType: 'json',
        cache: false,
        success:function(data){
          $("#loader").hide();
          $("#waitWords").hide();
          $("#thankYouWords").show();
          $("#thankYouWords").append("<p style='margin-top:20px;'>Address: "+address+"<br>Service: "+ serviceType+"<br> Date: "+date+"<br>Phone number: "+phone+"<br>Additional Requests: "+additionalThings);
          mixpanel.track("Thank You", {
            });
        },
        error: function(data){
          console.log(data);
          alert("Try Again");
          $("#orderForm").show();
        }
      });
      $("#orderForm").hide();
      $("#thankYou").show();
    }
  });
});
