<?php

/*
 * @author: Darwin Themes
 */
switch ($_POST['ajaxFct']) {
    case "ajx_send_contact_email":
        echo json_encode(send_contact_email());
        break;
    case "ajx_send_gaq_email":
        echo json_encode(send_gaq_email());
        break;
    case "ajx_save_file":
        echo json_encode(save_file());
        break;
}

function send_contact_email() {
    if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $_REQUEST['email'])) {
        $return["details"] = "Invalid email format.";
        $return["status"] = false;
        return $return;
    }
    $to = 'support@darwinthemes.com'; // emails where the email goes
    $subject = 'Contact form submission';
    $eol = PHP_EOL;

    $separator = md5(time());
    $message = "Name: " . $_REQUEST['name'] . "\r\n";
    if ($_REQUEST['company'] != "") {
        $message .= "Company: " . $_REQUEST['company'] . "\r\n";
    }
    if ($_REQUEST['phone'] != "") {
        $message .= "Phone: " . $_REQUEST['phone'] . "\r\n";
    }
    $message.= "\r\nMessage:\r\n\r\n";
    $message.= $_REQUEST['message'] . "\r\n";

    require dirname(__FILE__) . '/PHPMailer-master/PHPMailerAutoload.php';

    $mail = new PHPMailer();
    $mail->setFrom($_REQUEST['email'], $_REQUEST['name']);
    $mail->addReplyTo($_REQUEST['email'], $_REQUEST['name']);
    $mail->addAddress($to, '');
    $mail->Subject = $subject;

    $mail->Body = $message;
    $mail->AllowEmpty = true;
    $response['mail'] = $mail;
    if (!$mail->send()) {
        $return["details"] = $mail->ErrorInfo;
        $return["status"] = false;
    } else {
        $return["status"] = true;
    }
    return $return;
}

function save_file() {
    $return = array();
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $_FILES["file"]["name"])) {
        $return['status'] = true;
        $return['filepath'] = $_FILES["file"]["name"];
    } else {
        $return['status'] = false;
        $return['details'] = "upload failed";
    }
    return $return;
}

function send_gaq_email() {
    
    if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $_REQUEST['email'])) {
        $return["details"] = "Invalid email format.";
        $return["status"] = false;
        return $return;
    }
    $to = 'support@darwinthemes.com'; // emails where the email goes
    $subject = 'Get a quote form submission';
    $eol = PHP_EOL;
    $filename = $_REQUEST['file'];

    $separator = md5(time());
    $filename = $_REQUEST['file'];
    $message = "Name: " . $_REQUEST['name'] . "\r\n";
    if ($_REQUEST['company'] != "") {
        $message .= "Company: " . $_REQUEST['company'] . "\r\n";
    }
    if ($_REQUEST['phone'] != "") {
        $message .= "Phone: " . $_REQUEST['phone'] . "\r\n";
    }
    $message.= "\r\nMessage:\r\n\r\n";
    $message.= $_REQUEST['message'] . "\r\n";

    require dirname(__FILE__) . '/PHPMailer-master/PHPMailerAutoload.php';

    $mail = new PHPMailer();
    $mail->setFrom($_REQUEST['email'], $_REQUEST['name']);
    $mail->addReplyTo($_REQUEST['email'], $_REQUEST['name']);
    $mail->addAddress($to, '');
    $mail->Subject = $subject;

    $mail->Body = $message;
    $mail->AllowEmpty = true;
    $mail->addAttachment(dirname(__FILE__) . "/" . $filename);
    $response['mail'] = $mail;
    if (!$mail->send()) {
        $return["details"] = $mail->ErrorInfo;
        $return["status"] = false;
    } else {
        $return["status"] = true;
    }
    return $return;
}
